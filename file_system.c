/*
 * Name: Fernando Do Nascimento
 * ID #: 1000638103
 * Programming Assignment 4
 * Description: This file cotains a program that performs all of the functions stated in the
 * Programming Assignment 4.
 */

/* 
 * Source
 * ------
 * 
 * Some of the code (i.e. put and get) was provided by professor Bakker during the lecture
 * and a sample file was uploaded to Blackboard to use as reference.
 * 
 * For displaying the time in the function list I used the following reference to display 
 * the date: http://www.tutorialspoint.com/c_standard_library/c_function_strftime.htm
 *
 * For determining if the file name is alphanumeric I used the following code as reference:
 * http://www.codingunit.com/c-reference-alphanumeric-test-isalnum  
 */

//Libraries needed to complete this Lab Assignment
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <ctype.h>

//Constant variables used for this Lab Assignment
#define BLOCK_SIZE 	2048
#define NUM_BLOCKS 	5120
#define MAX_FILE_NAME 	255
#define MAX_FILES 	128
#define COMMAND 	5
#define PARAMETER 	255
#define MAX_INPUT 	515
#define MAX_FILES_SIZE 	98304
#define BLOCKS_PER_FILE 48

struct inode
{
	int used_block[NUM_BLOCKS];
	int blocks[NUM_BLOCKS];
	int valid;
	int bytes;
	time_t time_created;
	char filename[MAX_FILE_NAME];
};


struct directory_structure
{
	struct inode files[MAX_FILES];
};

struct directory_structure directory;

//2D unsigned char array of size 2048x5120 (10MB)
unsigned char data_blocks[NUM_BLOCKS][BLOCK_SIZE];

//Function Prototypes
int putCommand(char *filename);
int findNextUnusedBlock();
void listCommand();
int dfCommand(char *command);
int getCommandTwo(char *filenameOne, char *filenameTwo);
int getCommandOne(char *filename);
int delCommand(char *filename);

int main(void)
{
	int done = 0;
	
	//Runs infinite loop and reads input from the user for the shell.
	while(!done)
	{
		char input[MAX_INPUT], command[COMMAND], parameterOne[PARAMETER], parameterTwo[PARAMETER];
		int counter = 0;
		int i, j, fileSizeOne, fileSizeTwo, validFileSize, validNameOne, validNameTwo;	

		validFileSize  = validNameOne = validNameTwo = 0;	
		
		printf("mfs> ");
		fflush(NULL);
		
		//Reads input from the user and stores it in the input char array.
		fgets(input, MAX_INPUT, stdin);
	
		//If user enters a new line without any commands or parameters, then the program will not execute any
		//operations and the shell will reappear until a command is entered.		
		if(strcmp(input, "\n") != 0)
		{	
			//Removes the NULL character from the input char array.
			input[strlen(input) - 1] = '\0';
		

			//Tokenizes the string in order to separate the input in different parts
			char *temp = strtok(input, " ");

			//Copies the tokenized string value in the command char array.
			//The first input parameter is the command supported by the shell				
			strcpy(command, temp);
	
			//If the command entered by the user is put, get or del. The program will keep tokenizing the
			//input array since those commands have one or more parameters.
			if(strcmp(command, "put") == 0 || strcmp(command, "get") == 0 || strcmp(command, "del") == 0)
			{
				//Keeps tokenizing the character array until it reaches the end.
				//There is a counter array that keeps track of the parameters in the
				//input char array in order to know if the command has more than one
				//parameter.
				while(temp != NULL)
				{
					temp = strtok(NULL, " ");
					
					if(temp != NULL)
					{
						if(counter == 0)
							strcpy(parameterOne, temp);
	
						if(counter == 1)
							strcpy(parameterTwo, temp);

						counter++;
					}
				}	
			}
			
			//Error checking for commands with only one parameter 
			if(counter == 1)
			{
				fileSizeOne = strlen(parameterOne);

				if(fileSizeOne > 255)
				{
					printf("msh> %s error: File name too long.\n", command);
					validFileSize = (-1);
				}

				//Checks if the file name is alphanumeric or not
				for(i = 0;i < strlen(parameterOne);i++)
				{
					if(isalnum(parameterOne[i]))
						;

					else
					{
						if(parameterOne[i] == '.')
							;

						else
							validNameOne = (-1);
					}
				}

				if(validNameOne == -1)
					printf("msh> %s error: File name is not alphanumeric.\n", command);
			}
	
			//Error checking for commands with more than one parameter
			if(counter == 2)
			{
				fileSizeOne = strlen(parameterOne);
				fileSizeTwo = strlen(parameterTwo);
				
				//Checks for valid file sizes
				if(fileSizeOne > 255 || fileSizeTwo > 255)
				{
					printf("msh> %s error: File name too long.\n", command);
					validFileSize = (-1);
				}

				//Checks if the first file name entered is alphanumeric or not
				for(i = 0;i < strlen(parameterOne);i++)
				{
					if(isalnum(parameterOne[i]))
						;

					else
					{
						if(parameterOne[i] == '.')
							;

						else
							validNameOne = (-1);
					}
				}

				//Checks if the second file name entered is alphanumeric or not
				for(j = 0;j < strlen(parameterTwo);j++)
				{
					if(isalnum(parameterTwo[j]))
						;

					else
					{
						if(parameterTwo[j] == '.')
							;

						else
							validNameTwo = (-1);
					}
				
				}
			
				//Displays error message to the user
				if(validNameOne == (-1) || validNameTwo == (-1))
					printf("msh> %s error: File name is not alphanumeric.\n", command);
			}
		}

		//Commands that do not a parameter will be executed inside this statement.
		if(strcmp(command, "list") == 0 || strcmp(command, "df") == 0)
		{
			if(strcmp(command, "list") == 0)
				listCommand();

			if(strcmp(command, "df") == 0)
				dfCommand(command);
		}

		//Commands that have one or more parameters will be executed inside this statement.
		if(strcmp(command, "put") == 0 || strcmp(command, "get") == 0 || strcmp(command, "del") == 0)
		{
			//If the commands has one parameter and have a valid name and size,
			//then the commands will be executed.
			if(counter == 1 && validFileSize != (-1) && validNameOne != (-1))
			{
				if(strcmp(command, "get") == 0)
					getCommandOne(parameterOne);
				
				if(strcmp(command, "put") == 0)
					putCommand(parameterOne);

				if(strcmp(command, "del") == 0)
					delCommand(parameterOne);

			}

			//If the command has two parameters and have a valid name and size,
			//then the command will be executed.
			if(counter == 2 && validFileSize != (-1) && validNameOne != (-1) && 
				validNameTwo != (-1))
			{
				if(strcmp(command, "get") == 0)
					getCommandTwo(parameterOne, parameterTwo);
			}
		}	
	}
	
	return 0;
}

/*
 * Function: put_command
 * Parameter(s): fileName - Pointer of type char that points to the address containing the name 
 * of the file.
 * Returns: 0 if there was an error in the function. 1 if the function was completed successfully.
 * Description: The function checks if the file name entered by the user exits or not. If the file
 * does not exist.
 * If the file exists, then the function will 
 */
int putCommand(char *filename)
{
	//Verifies if the file exists.
	struct stat buf;
	int status;

	status = stat(filename, &buf);

	if(status == (-1))
	{
		printf("msh> put error: File does not exist.\n");
		return 0;
	}
        
	FILE *fp = fopen(filename, "r");

	int file_entry = (-1);
	int i;

	//Add the file to the directory structure
	for(i = 0;i < MAX_FILES;i++)
	{
		if(directory.files[i].valid == 0)
		{
			file_entry = i;
			break;
		}
	}

	if(file_entry == (-1))
	{
		printf("msh> put error: Error occured allocation a directory entry.\n");
		return 0;
	}
	
	directory.files[file_entry].valid = 1;

	struct timeval tv;
	gettimeofday(&tv, NULL);

	directory.files[file_entry].time_created = tv.tv_sec;

	memcpy(directory.files[file_entry].filename, filename, sizeof(filename) + 1);

	directory.files[file_entry].bytes = buf.st_size;

	//Initialize index variables to zero.
	int copy_size = buf.st_size;

	//We want to copy and write chunks of BLOCK_SIZE. In order to do this we are
	//going to use fseek to move along out file stream in chunks of BLOCK_SIZE.
	//We will copy bytes, increment the file pointer by BLOCK_SIZE and repeat.
	int offset = 0;
	int block_count = 0;
	int block_index = findNextUnusedBlock();
	int free_space = dfCommand("put");

	while(copy_size > 0)
	{
		fseek(fp, offset, SEEK_SET);
		
		//Read BLOCK_SIZE number of bytes from the input file and store them
		//in the data array.
		int bytes = fread(data_blocks[block_index], BLOCK_SIZE, 1, fp);
		
		//Checks if there is enough space in the file system to store
		//the file entered by the user.
		if(bytes > free_space)
		{
			printf("msh> put error: Not enough disk space.\n");
			return 0;
		}

		//Mark block used
		directory.files[file_entry].used_block[block_index] = 1;

		if(bytes == 0 && !feof(fp))
		{
			printf("An error occured reading from the inputt file.\n");
			return (-1);
		}

		//Clear the EOF file flag.
		clearerr(fp);

		//Reduce copy size by the BLOCK_SIZE bytes.
		copy_size -= BLOCK_SIZE;

		offset += BLOCK_SIZE;

		directory.files[file_entry].used_block[block_count++] = block_index;

		//Increment the index into the block array
		block_index = findNextUnusedBlock();

		//Finised copying from the input file, so we close the file.
		fclose(fp);
	}

	return 1;
}

/*
 * Function: findNextUnusedBlock
 * Parameter(s): none.
 * Returns: i - Index in blocks array where the next available block is located.
 * 0 - No unused block available.
 * Description: The program performs a linear search looking for the next available
 * block in the block array. It will either return an index of the next available block
 * or a zero if the is no block available.
 */
int findNextUnusedBlock()
{
	int i;

	for(i = 0;i < NUM_BLOCKS;i++)	
	{
		if(directory.files[i].used_block[i] != 1)
			return i;
	}
	
	return 0;
}

/*
 * Function: listCommand
 * Paramter(s): none.
 * Returns: void.
 * Description: The function will loop thru the entire struct array file and if
 * the element valid is equal to 1. This means that there is a file in the file
 * system and it will display certain information about the file(Number of bytes,
 * date file was created, and the file name). The function will also increment a
 * file counter in order to prevent the function from printing invalid file name
 * when the file system is full.
 * If the valid value equals to 0, then there are no files in the file system.
 */
void listCommand()
{
	int i, file_counter;

	file_counter = 0;

	for(i = 0;i < MAX_FILES;i++)
	{
		//The operation loops thru the entire struct array searching for files.
		//If the struct array files has a valid value of 1, then a file exists
		//in the file system.
		//After that the operation displays the number of bytes of the file,
  		//the date the file was created and the name of the file.
		//This operation will also increase a file_counter variable to indicate
		//that file system is not empty.
		if(directory.files[i].valid == 1)
		{
			char buff[20];
			strftime(buff, 20, "%b %d %H:%M", localtime(&directory.files[i].time_created));

			printf("%d\t%s\t%s\n", directory.files[i].bytes, buff, directory.files[i].filename);

			file_counter++;
		}
		
		//If the operation finds files with 0 bytes and a file_counter of 0, then
		//this means that there are no files inside the file system.
		if(directory.files[i].valid != 1 && file_counter == 0)
		{
			printf("msh> list: No files found.\n");
			break;
		}
	}
}

/*
 * Function: dfCommand
 * Parameter(s): command - pointer of type char that points to the address holding the 
 * command entered by the user.
 * Returns: free_space - The amount of bytes that are free in the file system.
 * Description: The function calculates how many bytes are free in the file system.
 */
int dfCommand(char *command)
{
	int i, free_space;

	free_space = MAX_FILES_SIZE;

	for(i = 0;i < MAX_FILES;i++)
	{
		if(directory.files[i].valid == 1)
			free_space -= directory.files[i].bytes;
	}

	
	if(strcmp(command, "df") == 0)
		printf("%d bytes free\n", free_space);

	return free_space;
}

/*
 * Function: getCommandOne
 * Parameter(s): filename - pointer of type that the points the the filename address.
 * Returns: 1 - If operations in the function are executed successfully.
 * 0 - If operations in the function are unsuccessful.
 * Description: The function first checks if the filename entered by the user exists.
 * If the file does not exists, then the function will display a message and return to
 * the main.
 * If the file does exists, then the function will copy the information of the file
 * inside the file system to the file and store it in the current working directory.
 */
int getCommandOne(char *filename)
{
	int i, j;
	int index;
	int validFile = 0;
	
	//Searches if the filename entered by the user exists.
	//If file exists, then the following operation will break from the
	//loop, storing the index where the file is located.
	for(i = 0;i < MAX_FILES;i++)
	{
		if(strcmp(filename, directory.files[i].filename) == 0)
		{
			index = i;
			validFile = 1;
			break;
		}

		else
			validFile = (-1);
	}

	//The file was not found
	if(validFile == (-1))
	{
		printf("get error: File not found.\n");
		return 0;
	}

	//If the file was found, then the information of the file stored in the file system
	//will be saved in the file entered by the user.
	else
	{
		FILE *of = fopen(filename, "w");

		int copy_size = directory.files[index].bytes;
		int block_index = 0;

		//Copies the information from the file system into the file in the current
		//working directory.
		while(copy_size > 0)
		{
			int num_bytes;

			if(copy_size < BLOCK_SIZE)
				num_bytes = copy_size;

			else
				num_bytes = BLOCK_SIZE;
			
			block_index = directory.files[index].used_block[j];
			
			fwrite(data_blocks[block_index], num_bytes, 1, of);
			
			copy_size -= BLOCK_SIZE;
			
			j++;
		}

		fclose(of);
	}

	return 1;
}	

/*			
 * Function: getCommandTwo
 * Parameter(s): filenameOne - pointer of type char that holds the address of the first
 * file in the file system.
 * filenameTwo - pointer of type char that holds the address of the file that will store
 * the information from the first file.
 * Returns: 0 - If an operation in the function failed.
 * 1 - If all the operations were successful.
 */
int getCommandTwo(char *filenameOne, char *filenameTwo)
{
	int status;
	struct stat buf;

	status = stat(filenameOne, &buf);

	if(status == (-1))
	{
		printf("msh> get error: File not found.\n");
		return 0;
	}

	else
	{
		FILE *fp = fopen(filenameOne, "r");
	
		//Size of input file		
		int copy_size = buf.st_size;

		//Used to copy and write chunks of BLOCK_SIZE
		int offset = 0;

		int block_index = 0;

		//copy_size equals to the size of the input file.
		//The function will copy BLOCK_SIZE byte from the file.
		while(copy_size > 0)
		{
			fseek(fp, offset, SEEK_SET);
			
			int bytes = fread(data_blocks[block_index], BLOCK_SIZE, 1, fp);

			if(bytes == 0 && !feof(fp))
			{
				printf("An error occured reading from the input file.\n");
				return 0;
			}

			clearerr(fp);

			copy_size -= BLOCK_SIZE;

			offset += BLOCK_SIZE;

			block_index++;
		}
		
		fclose(fp);

		//The following operations will store the information from the 
		//first file entered by the user to the second file entered by the
		//user.
		FILE *of = fopen(filenameTwo, "w");

		if(of == NULL)
		{
			printf("Could not open output file: %s\n", filenameTwo);
			perror("Opening output file returned");

			return 0;
		}

		block_index = 0;
		copy_size = buf.st_size;
		offset = 0;

		while(copy_size > 0)
		{
			int num_bytes;

			if(copy_size < BLOCK_SIZE)
				num_bytes = copy_size;

			else
				num_bytes = BLOCK_SIZE;

			//Writes the information from the file one file to another.
			fwrite(data_blocks[block_index], num_bytes, 1, of);

			copy_size -= BLOCK_SIZE;
			offset += BLOCK_SIZE;
			block_index++;

			fseek(of, offset, SEEK_SET);
		}

		fclose(of);
	}

	return 1;
}

/*
 * Function: delCommand
 * Parameter(s): filename - pointer of type char that holds the address of the filename
 * to be deleted.
 * Returns: 1 - If the file is successfully removed from the file system.
 * 0 - If the file is not found in the file system.
 * Description: The function will check if the file entered by the user is located inside
 * the file system.
 * If the file is found in the file system, then the function will continue to perform the
 * deleting of the file in the file system. Once the deletion is completed, the program
 * will return to the main.
 * If the file is not found in the file system, the function will display a message to the
 * user and return to the main.
 */
int delCommand(char *filename)
{
	int i, j;
	int index, validFile;
	
	//Checks if the file is in the file system.
	for(i = 0;i < MAX_FILES;i++)
	{
		//If the file is found, then the index of the location in the struct array
		//will be stored in the variable index and it would break from loop.
		if(strcmp(filename, directory.files[i].filename) == 0)
		{
			index = i;
			validFile = 1;
			break;
		}

		//File not found
		else
			validFile = (-1);
	}

	//File not found
	//Displays message to the user and returns to the main.
	if(validFile == (-1))
	{
		printf("msh> del error: File not found.\n");
		return 0;
	}

	//The file is found and deleted from the file system
	else
	{
		directory.files[index].valid = 0;

		for(j = 0;j < BLOCKS_PER_FILE;j++)
			directory.files[index].used_block[j] = 0;
	}

	return 1;
}

